/*
 * Copyright (c) 2015 Dmitriy Shestakov
 * This file is part of tetris_made_for_practice.
 *
 * tetris_made_for_practice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "MainUI.h"
#include <QMessageBox>
#include <fstream>
#include <math.h>
//#include <string>
//#include <streambuf>

MainUI::MainUI(Game *game, QWidget *parent)
        : QWidget(parent), _game(game) {
    setWindowTitle("Tetris");
    setWindowIcon(QIcon("icon.png"));
    setFixedSize(340, 455);
    setStyleSheet("QWidget { background: #333; color: #7d7; }"
                  "QSpinBox { color: #3d3; background: #292929; border: 0px; font: bold }"
                  "QPushButton { color: #3d3; background: transparent; "
                  "border: 1px solid #393; border-radius: 6px; font: bold; width: 60; }"
                  "QPushButton:pressed { color: #222; background: #3d3; }");

    startTimer(16);
    _level = new QSpinBox(this);
    _level->setButtonSymbols(QSpinBox::NoButtons);
    _level->move(width() - 55, 157);
    _level->setValue(_game->level);
    _level->setMaximum(99);
    _level->setFocusPolicy(Qt::NoFocus);
    _helpBtn = new QPushButton("?", this);
    _helpBtn->setFixedSize(20, 20);
    _helpBtn->move(315, 5);
    _helpBtn->setFocusPolicy(Qt::NoFocus);
    QPushButton *startBtn = new QPushButton("Start", this);
    startBtn->move(width()-110, 220);
    startBtn->setFlat(true);
    startBtn->setFocusPolicy(Qt::NoFocus);
    QPushButton *pauseBtn = new QPushButton("Pause", this);
    pauseBtn->move(width()-110, 240);
    pauseBtn->setFlat(true);
    pauseBtn->setFocusPolicy(Qt::NoFocus);

    _scorePosX = _scorePosY = _scoreSize = 0.0f;
    _score = 0;
    _scoreOpacity = 0.0f;
    _curKey = -1;
    _clockwise = true;

    // connections
    connect(_helpBtn, SIGNAL(clicked()), this, SLOT(showHelp()));
    connect(startBtn, SIGNAL(clicked()), this, SLOT(start()));
    connect(pauseBtn, SIGNAL(clicked()), this, SLOT(pause()));
}

MainUI::~MainUI() {
    delete _game;
}

// --------------------- EVENTS -------------------------

void MainUI::paintEvent(QPaintEvent *) {
    QPainter p;
    p.begin(this);
    p.setRenderHint(QPainter::Antialiasing);
    p.setOpacity(1);

    // title
    QFont font = QFont("Consolas", 40);
    font.setBold(true);
    p.setFont(font);
    p.setPen(QPen("#131"));
    p.drawText(72, 42, "ТЕТРИС");
//    p.drawText(62, 42, "TETRIS");
    p.setPen(QPen("#3c3"));
    p.drawText(70, 40, "ТЕТРИС");
//    p.drawText(60, 40, "TETRIS");

    // drawing borders
    p.drawRect(10, 50, 200, 400);
    p.drawRect(230, 50, 100, 100);

    // next shape
    p.setPen(QPen("#393"));
    p.setBrush(QBrush("#3d3"));
    if (_game->nextShape != NULL) {
        Point *pt = &_game->nextShape->p0;
        Point c = _game->nextShape->getCenter(20);
        Point pos = _game->nextShape->pos;
        for (int i = 0; i < 4; ++i) {
            int x = pos.x - c.x + pt[i].x * 20;
            int y = pos.y + c.y - pt[i].y * 20;
            p.drawRect(x - 10, y - 10, 20, 20);
        }
    }

    // current shape
    if (_game->curShape != NULL) {
        Point *pt = &_game->curShape->p0;
        for (int i = 0; i < 4; ++i) {
            int x = (_game->curShape->pos.x + pt[i].x) * 20 + 10;
            int y = 430 - (_game->curShape->pos.y + pt[i].y) * 20;
            if (y > 40)
                p.drawRect(x, y, 20, 20);
        }
    }

    // built
    p.setPen(QPen("#3d3"));
    p.setBrush(QBrush("#393"));
    for (int y = 0; y < 22; ++y) {
        for (int x = 0; x < 10; ++x) {
            int yPos = 430 - y * 20;
            if (_game->grid[y][x] && yPos > 40)
                p.drawRect(x * 20 + 10, yPos, 20, 20);
        }
    }

    // info
    if (_game->curShape != NULL && _game->paused) {
        font.setPixelSize(20);
        p.setFont(font);
        p.setPen(QPen("#131"));
        p.drawText(71, height() / 2 + 1, "PAUSED");
        p.setPen(QPen("#5f5"));
        p.drawText(70, height() / 2, "PAUSED");
    }
    if (_game->gameOver) {
        font.setPixelSize(30);
        p.setFont(font);
        p.setPen(QPen("#131"));
        p.drawText(23, height() / 2 + 1, "GAME OVER!");
        p.setPen(QPen("#5f5"));
        p.drawText(22, height() / 2, "GAME OVER!");
    }
    paintButtons(p);
    paintButtonsPress(p);
    paintScore(p);
    if (_game->levelUp > 0.0f)
        paintLevelUp(p);
    p.end();
}

void MainUI::keyPressEvent(QKeyEvent *evt) {
    _curKey = evt->key();
    update();
    switch (evt->key()) {
        case Qt::Key_Escape:
            this->close();
            break;

        case Qt::Key_F1:
            showHelp();
            break;

        case Qt::Key_P:
            pause();
            break;

        case Qt::Key_Space:
        case Qt::Key_Return:
        case Qt::Key_Enter:
            start();

        default:
            break;
    }

    if (_game->hold > 0.0f || _game->curShape == NULL || _game->paused) return;

    switch (evt->key()) {
        case Qt::Key_Q:
            _game->curShape->rotate(false);
            break;

        case Qt::Key_Up:
        case Qt::Key_W:
            _game->curShape->rotate(_clockwise);
            break;

        case Qt::Key_E:
            _game->curShape->rotate(true);
            break;

        case Qt::Key_Left:
        case Qt::Key_A:
            _game->curShape->trLeft = true;
            _game->curShape->translate(&_game->grid[0][0]);
            _game->curShape->hold = 1.0f;
            break;

        case Qt::Key_Right:
        case Qt::Key_D:
            _game->curShape->trRight = true;
            _game->curShape->translate(&_game->grid[0][0]);
            _game->curShape->hold = 1.0f;
            break;

        case Qt::Key_Down:
        case Qt::Key_S:
            _game->curShape->drop = true;
            break;

        default:
            break;
    }
}

void MainUI::keyReleaseEvent(QKeyEvent *evt) {
    _curKey = -1;
    if (_game->curShape == NULL) return;

    _game->curShape->hold = 0.0f;
    if (evt->key() == Qt::Key_Down || evt->key() == Qt::Key_S)
        _game->curShape->drop = false;
    if (evt->key() == Qt::Key_Left || evt->key() == Qt::Key_A)
        _game->curShape->trLeft = false;
    if (evt->key() == Qt::Key_Right || evt->key() == Qt::Key_D)
        _game->curShape->trRight = false;
}

void MainUI::mousePressEvent(QMouseEvent *event) {
    int xp = QCursor().pos().x();
    int yp = QCursor().pos().y();
    if ( xp > x() + 266 && xp < x() + 296 && yp > y() + 397 && yp < y() + 427) {    // y can be different on windows
        _clockwise = !_clockwise;
        update();
    }
    QWidget::mousePressEvent(event);
}

void MainUI::timerEvent(QTimerEvent *) {
    _game->update();
    if (!_game->paused && !_game->gameOver && _level->value() != _game->level)
        _level->setValue(_game->level);
    update();
}

// --------------------- METHODS -------------------------

void MainUI::moveScore() {
    if (_game->addedPoints > 0) {
        _score = _game->addedPoints;
        _game->addedPoints = 0;
        _scorePosX = 75.0f;
        _scorePosY = 430.f - 20.f * _game->clearedAveragePos - 10.f;
    }

    if (_scoreOpacity > 0.0f) {
        _scoreOpacity -= 0.07f;
        return;
    }
    float xPos = float(width()) - 110.0f - 75.0f;
    float yPos = 205.0f - (430.0f - 20.0f * _game->clearedAveragePos - 10.0f);
    float len = sqrtf(xPos * xPos + yPos * yPos);
    _scorePosX += xPos/len * 5.0f;
    _scorePosY += yPos/len * 5.0f;
    _scoreSize = 50.0f * sinf((_scorePosX-75.0f)/(xPos/3.0f)) + 10.0f;
    if (abs(_scorePosY - 205.0f) < 5.0f) {
        _game->score += _score;
        _score = 0;
        _scoreOpacity = 1.0f;
        _scorePosY = 0.0f;
    }
}

void MainUI::paintScore(QPainter &p) {

    QFont font = p.font();
    font.setBold(true);
    p.setPen(QPen("#3d3"));
    font.setPixelSize(14);
    p.setFont(font);
    p.drawText(width() - 110, 170, QString("Level:"));
    p.drawText(width() - 110, 190, QString::fromStdString("Score:"));
    p.drawText(width() - 110, 205, QString::fromStdString(_game->getScoreStr()));

    moveScore();
    if (_score > 0) {
        font.setPixelSize(int(_scoreSize));
        p.setFont(font);
        p.setPen(QPen("#3d3"));
        p.drawText(int(_scorePosX)+10, int(_scorePosY), QString("%1").arg(_score));
    }

    if (_scoreOpacity > 0.0f) {
        p.setPen(QPen("#3d3"));
        font.setPixelSize(int(32.0f - _scoreOpacity * 18.0f));
        p.setFont(font);
        p.setOpacity(_scoreOpacity);
        p.drawText(width() - 150 + int(40.0f *_scoreOpacity), 210 - int(5.0f *_scoreOpacity),
                   QString::fromStdString(_game->getScoreStr()));
    }
}

void MainUI::paintButtons(QPainter &p) {

    p.setPen(QPen("#777"));
    p.setBrush(QBrush("#292929"));
    p.drawRect(233, 367, 30, 30);
    p.drawRect(299, 367, 30, 30);
    p.drawRect(233, 400, 30, 30);
    p.drawRect(266, 400, 30, 30);
    p.drawRect(299, 400, 30, 30);
    p.setBrush(QBrush("#222"));
    p.drawRect(266, 367, 30, 30);

    QPen pen("#555");
    pen.setWidth(2);
    p.setPen(pen);
    p.setBrush(QBrush("#292929"));
    p.drawEllipse(241, 375, 13, 13);
    p.drawEllipse(307, 375, 13, 13);
    p.setBrush(QBrush("#222"));
    p.drawEllipse(274, 375, 13, 13);
    p.setBrush(QBrush("#292929"));
    p.setPen(Qt::NoPen);
    p.drawRect(238, 385, 20, 10);
    p.drawRect(304, 385, 20, 10);
    p.setBrush(QBrush("#222"));
    p.drawRect(269, 385, 20, 10);

    p.setPen(QPen("#555"));
    p.setBrush(QBrush("#444"));
    QPainterPath pp;
    pp.moveTo(238, 385);
    pp.lineTo(245, 381);
    pp.lineTo(245, 388);
    pp.closeSubpath();
    if (_clockwise) {
        pp.moveTo(290, 385);
        pp.lineTo(283, 381);
        pp.lineTo(283, 388);
    } else {
        pp.moveTo(271, 385);
        pp.lineTo(278, 381);
        pp.lineTo(278, 388);
    }
    pp.closeSubpath();
    pp.moveTo(323, 385);
    pp.lineTo(316, 381);
    pp.lineTo(316, 388);

    pp.closeSubpath();
    pp.moveTo(240, 415);
    pp.lineTo(254, 407);
    pp.lineTo(254, 423);
    pp.lineTo(240, 415);
    pp.closeSubpath();
    pp.moveTo(322, 415);
    pp.lineTo(308, 407);
    pp.lineTo(308, 423);
    pp.lineTo(322, 415);
    pp.closeSubpath();
    pp.moveTo(273, 409);
    pp.lineTo(289, 409);
    pp.lineTo(281, 423);
    pp.lineTo(273, 409);
    p.drawPath(pp);
}

void MainUI::paintButtonsPress(QPainter &p) {
    if (_curKey == -1) return;
    p.setOpacity(0.5f);
    QPen pen("#3d3");
    pen.setWidthF(1.5f);
    p.setPen(pen);
    p.setBrush(QBrush("#242"));
    QFont font = p.font();
    font.setPixelSize(20);
    p.setFont(font);
    switch (_curKey){
        case Qt::Key_Down:
            p.drawRect(266, 400, 30, 30);
            p.drawLine(281, 408, 281, 415);
            p.drawLine(277, 415, 285, 415);
            p.drawLine(277, 415, 281, 422);
            p.drawLine(285, 415, 281, 422);
            break;
        case Qt::Key_S:
            p.drawRect(266, 400, 30, 30);
            p.drawText(275, 421, "S");
            break;

        case Qt::Key_Up:
            p.drawRect(266, 367, 30, 30);
            p.drawLine(281, 389, 281, 382);
            p.drawLine(277, 382, 285, 382);
            p.drawLine(277, 382, 281, 375);
            p.drawLine(285, 382, 281, 375);
            break;
        case Qt::Key_W:
            p.drawRect(266, 367, 30, 30);
            p.drawText(275, 388, "W");
            break;

        case Qt::Key_Left:
            p.drawRect(233, 400, 30, 30);
            p.drawLine(256, 415, 248, 415);
            p.drawLine(248, 411, 248, 419);
            p.drawLine(248, 411, 240, 415);
            p.drawLine(248, 419, 240, 415);
            break;
        case Qt::Key_A:
            p.drawRect(233, 400, 30, 30);
            p.drawText(242, 421, "A");
            break;

        case Qt::Key_Right:
            p.drawRect(299, 400, 30, 30);
            p.drawLine(307, 415, 314, 415);
            p.drawLine(314, 411, 314, 419);
            p.drawLine(314, 411, 321, 415);
            p.drawLine(314, 419, 321, 415);
            break;
        case Qt::Key_D:
            p.drawRect(299, 400, 30, 30);
            p.drawText(308, 421, "D");
            break;

        case Qt::Key_Q:
            p.drawRect(233, 367, 30, 30);
            p.drawText(242, 388, "Q");
            break;

        case Qt::Key_E:
            p.drawRect(299, 367, 30, 30);
            p.drawText(308, 388, "E");
            break;

        default:
            break;
    }
    p.setOpacity(1.0f);
}

void MainUI::paintLevelUp(QPainter &p) {
    _game->levelUp -= 0.05f;
    QFont font = p.font();
    font.setBold(true);
    font.setPixelSize(int(50.0f - _game->levelUp * 20.0f));
    p.setFont(font);
    p.setPen(QPen("#3d3"));
    p.setOpacity(_game->levelUp);
    p.drawText(10 + int(40.0f *_game->levelUp), height()/2 - int(0.5f *_game->levelUp),
               QString("Level %1").arg(_game->level));
}

void MainUI::showHelp() {
    std::ifstream f("about");
    if (f) {
        std::string s ((std::istreambuf_iterator<char>(f)), std::istreambuf_iterator<char>());
        _game->paused = true;
        QMessageBox::information(this, "Info", QString::fromStdString(s));
        _game->paused = false;
    }
}

void MainUI::start() {
    _game->startGame(_level->value());
    setFocus();
}

void MainUI::pause() {
    _game->paused = !_game->paused;
    setFocus();
}
