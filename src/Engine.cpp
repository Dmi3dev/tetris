/*
 * Copyright (c) 2015 Dmitriy Shestakov
 * This file is part of tetris_made_for_practice.
 *
 * tetris_made_for_practice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Engine.h"
#include <unistd.h>
#include "ctime"
#include <thread>
#include <sstream>
#include <iomanip>

Game::Game() {
    nextShape = NULL;
    curShape = NULL;
    paused = true;
    gameOver = false;
    score = 0;
    addedPoints = 0;
    cleared = 0;
    hold = 0.0f;
    level = 1;
    levelUp = 0.0f;
    clearGrid();
}

Game::~Game() {
    delete nextShape;
    delete curShape;
}

void Game::startGame(int level_) {
    if (gameOver || curShape == NULL) {
        clearGrid();
        genNextShape();
        curShape->setPos(4, 19);
        paused = false;
        gameOver = false;
        level = level_;
        score = 0;
        addedPoints = 0;
        clearedAveragePos = 0;
    }
}

void Game::genNextShape() {
    if (curShape != NULL)
        delete curShape;

    if (nextShape != NULL) {
        curShape = nextShape;
    } else {
        curShape = new Shape();
        sleep(1);
    }
    curShape->setPos(4, 20);
    curShape->checkCollision(&grid[0][0]);
    if (curShape->landed) {
        gameOver = true;
    }
    curShape->hold = 1.0f;
    nextShape = new Shape(280, 100);
}

void Game::update() {
    if (curShape == NULL || paused || gameOver) return;
    static double lastTime = _timer.getCurTime();
    double curTime = _timer.getCurTime();
    hold -= 0.05f;
    if (cleared > 10*level) {
        levelUp = 2.0f;
        level++;
        cleared -= 10*level/2;
    }
    curShape->hold -= 0.05f;
    curShape->translate(&grid[0][0]);

    if (hold <= 0.0f && curTime - lastTime >= 1.0/( (1.0 + double(level))*0.5 )) {
        curShape->checkCollision(&grid[0][0]);
        if (curShape->landed) {
            Point *pt = &curShape->p0;
            for (int i = 0; i < 4; ++i) {
                int x = curShape->pos.x + pt[i].x;
                int y = curShape->pos.y + pt[i].y;
                grid[y][x] = 1;
            }
            threadedCL();
            genNextShape();
        }
        curShape->pos.y--;
        lastTime = curTime;
    }
}

void Game::clearLines() {
    vector<int> ys;
    float sum = 0.0f;
    for (int y = 0; y < 22; ++y) {
        bool all = true;
        for (int x = 0; x < 10; ++x) {
            if (!grid[y][x]) {
                all = false;
                break;
            }
        }
        if (all) {
            ys.push_back(y);
            cleared++;
            sum += float(y);
        }
    }
    clearedAveragePos = sum / float(ys.size());
    addedPoints = int(ys.size() * ys.size() * 100 * level);
    int shift = 0;
    for (int i : ys) {
        hold = 1.5f;
        i -= shift++;
        for (int x = 0; x < 10; ++x) {
            grid[i][x] = false;
            _timer.tick(50);
        }
        for (int y = i; y < 21; ++y) {
            for (int x = 0; x < 10; ++x) {
                grid[y][x] = grid[y+1][x];
            }
        }
    }
}

void Game::threadedCL() {
    std::thread t(&Game::clearLines, this);
    t.detach();
}

string Game::getScoreStr() {
    std::ostringstream ss;
    ss << std::setw(7) << std::setfill('0') << score;
    return ss.str();
}

void Game::clearGrid() {
    for (int y = 0; y < 22; ++y) {
        for (int x = 0; x < 10; ++x) {
            grid[y][x] = 0;
        }
    }
}


// --------------------- SHAPE ---------------------

Shape::Shape() {
    // doing random in decimals to get more random
    drop = false;
    trRight = false;
    trLeft = false;
    landed = false;
    hold = 0.0f;

    srand((unsigned int) time(0));
    int t = rand() % 70;
    type = (Type) (t / 10);

    srand((unsigned int) t);
    if (type == BLOCK) rot = 0;
    else if (type == STICK || type == SSHAPE || type == ZSHAPE) {
        rot = (rand() % 20) / 10;
    } else {
        rot = (rand() % 40) / 10;
    }
    _initPoints();
    _rotatePoint(p0);
    _rotatePoint(p1);
    _rotatePoint(p2);
    _rotatePoint(p3);
}

Shape::Shape(int X, int Y) {
    *this = Shape();
    pos.x = X;
    pos.y = Y;
}

void Shape::setPos(int X, int Y) {
    pos.x = X;
    pos.y = Y;
}

void Shape::rotate(bool left) {
    if (type == BLOCK) return;

    if (left) rot--;
    else rot++;

    if (rot > 3) rot = 0;
    else if (rot < 0) rot = 3;

    _initPoints();
    _rotatePoint(p0);
    _rotatePoint(p1);
    _rotatePoint(p2);
    _rotatePoint(p3);

    Point *pt = &p0;
    for (int i = 0; i < 4; ++i) {
        int y = pos.y + pt[i].y;
        int x = pos.x + pt[i].x;
        if (x < 0 || x > 9 || y < 0){
            rotate(!left);
            break;
        }

    }

}

// ------------------- PRIVATE ------------------------

void Shape::_rotatePoint(Point &p) {
    // rotation formula comes from 2:2 matrix rotation, simplified for needed cases
    Point newPoint;
    switch (rot) {
        case 0:
            newPoint.x = p.x;
            newPoint.y = p.y;
            break;
        case 1:
            newPoint.x = -p.y;
            newPoint.y =  p.x;
            if (type == STICK) newPoint.y += 1;
            break;
        case 2:
            newPoint.x = -p.x;
            newPoint.y = -p.y;
            if (type == STICK) newPoint.x -= 1;
            if (type == STICK) newPoint.y += 1;
            break;
        case 3:
            newPoint.x =  p.y;
            newPoint.y = -p.x;
            if (type == STICK) newPoint.x -= 1;
            break;
        default:
            break;
    }
    p.x = newPoint.x;
    p.y = newPoint.y;
}

void Shape::_initPoints() {
    switch (type) {
        case BLOCK:
            p0 = Point(-1, 1);
            p1 = Point(-1, 0);
            p2 = Point(0, 1);
            p3 = Point(0, 0);
            break;

        case STICK:
            p0 = Point(0,  2);
            p1 = Point(0,  1);
            p2 = Point(0,  0);
            p3 = Point(0, -1);
            break;

        case LSHAPE:
            p0 = Point( 0,  1);
            p1 = Point( 0,  0);
            p2 = Point( 0, -1);
            p3 = Point( 1, -1);
            break;

        case JSHAPE:
            p0 = Point( 0,  1);
            p1 = Point( 0,  0);
            p2 = Point( 0, -1);
            p3 = Point(-1, -1);
            break;

        case ZSHAPE:
            p0 = Point(-1, 1);
            p1 = Point( 0, 1);
            p2 = Point( 0, 0);
            p3 = Point( 1, 0);
            break;

        case SSHAPE:
            p0 = Point( 1, 1);
            p1 = Point( 0, 1);
            p2 = Point( 0, 0);
            p3 = Point(-1, 0);
            break;

        case TSHAPE:
            p0 = Point( 0, 1);
            p1 = Point( 0, 0);
            p2 = Point(-1, 0);
            p3 = Point( 1, 0);
            break;

        default:
            break;
    }
}

void Shape::checkCollision(bool *grid) {
    Point *pt = &p0;
    for (int i = 0; i < 4; ++i) {
        int y = pos.y + pt[i].y;
        int x = pos.x + pt[i].x;
        int p = x + y * 10;

        if (y <= 0 || grid[p - 10]) {
            landed = true;
            drop = false;
        }
        if (x <= 0 || grid[p - 1])
            trLeft = false;
        if (x >= 9 || grid[p + 1])
            trRight = false;
    }
}

void Shape::translate(bool *grid) {
    landed = false;
    checkCollision(grid);
    if (hold <= 0.0f) {
        if (trLeft) pos.x--;
        if (trRight) pos.x++;
        if (drop) pos.y--;
    }
}

Point Shape::getCenter(int block_len) {
    Point *pt = &p0;
    Point topLeft(pt->x*block_len - block_len/2, pt->y*block_len + block_len/2);
    Point botRight(pt->x*block_len + block_len/2, pt->y*block_len - block_len/2);
    for (int i = 1; i < 4; ++i) {
        topLeft.x  = min(topLeft.x, pt[i].x*block_len - block_len/2);
        topLeft.y  = max(topLeft.y, pt[i].y*block_len + block_len/2);
        botRight.x = max(botRight.x, pt[i].x*block_len + block_len/2);
        botRight.y = min(botRight.y, pt[i].y*block_len - block_len/2);
    }

    Point p = (topLeft + botRight);
    p /= 2.0f;
    return p;
}


