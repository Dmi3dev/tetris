/*
 * Copyright (c) 2015 Dmitriy Shestakov
 * This file is part of tetris_made_for_practice.
 *
 * tetris_made_for_practice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "Timer.h"
#include <stddef.h>

unsigned int Timer::FPS = 0;

Timer::Timer()
{
    _curTime = getCurTime();
    _lastTime = getCurTime();
    _elapsed = 0;
    _fpsCount = 0;
}

Timer::~Timer()
{

}

double Timer::getCurTime()
{
    gettimeofday(&_nowTime, NULL);
    return ((_nowTime.tv_sec * 1000000.0) + _nowTime.tv_usec) * 0.000001;
}

void Timer::tick(double lock_fps)
{
    double elapsed;
    _curTime = getCurTime();
    elapsed = _curTime - _lastTime;

    if (lock_fps > 0.0)
    {
        FPS = (unsigned int)lock_fps;
        while (elapsed < (1.0 / lock_fps))
        {
            _curTime = getCurTime();
            elapsed = _curTime - _lastTime;
        }
    }
    _lastTime = _curTime;

    _fpsCount++;
    _elapsed += elapsed;
    if (_elapsed > 1.0)
    {
        FPS = _fpsCount;
        _fpsCount = 0;
        _elapsed = 0.0;
    }
}
