/*
 * Copyright (c) 2015 Dmitriy Shestakov
 * This file is part of tetris_made_for_practice.
 *
 * tetris_made_for_practice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <iostream>
#include <QApplication>
#include <QWidget>
#include "MainUI.h"

using namespace std;

int main(int argc, char *argv[])
{
    cout << "Tetris v1.0.1" << endl;

    QApplication app(argc, argv);
    Game *game = new Game;

    MainUI win(game);
    win.show();
    win.move(400, 200);



    return app.exec();
}