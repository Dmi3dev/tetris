/*
 * Copyright (c) 2015 Dmitriy Shestakov
 * This file is part of tetris_made_for_practice.
 *
 * tetris_made_for_practice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */


#pragma once

#include "Timer.h"
#include <iostream>
#include <vector>

using namespace std;

struct Point;

class Shape;

class Game {
    Timer _timer;
public:
    Game();
    ~Game();

    void genNextShape();
    void update();
    void clearLines();
    void threadedCL();
    void clearGrid();
    void startGame(int level_ = 1);

    string getScoreStr();

    Shape *nextShape;
    Shape *curShape;
    int level;
    float levelUp;
    bool grid[22][10];
    bool paused;
    float hold;
    int score;
    int addedPoints;
    int cleared;
    bool gameOver;
    float clearedAveragePos;
};

struct Point {
    Point() {
        x = 0;
        y = 0;
    }

    Point(const Point &p) : x(p.x), y(p.y) { }
    Point(int X, int Y) : x(X), y(Y) { }
    Point operator+(const Point &rhs) const {
        Point p;
        p.x = x + rhs.x;
        p.y = y + rhs.y;
        return p;
    }

    void operator/=(const float f) {
        x /= f;
        y /= f;
    }

    int x;
    int y;
};

class Shape {
public:
    enum Type {
        BLOCK = 0,
        STICK,
        LSHAPE,
        JSHAPE,
        ZSHAPE,
        SSHAPE,
        TSHAPE,
    };

    Shape();

    Shape(int X, int Y);

    void setPos(int X, int Y);

    void rotate(bool left);

    void translate(bool *grid);

    void checkCollision(bool *grid);

    Point getCenter(int block_len);

private:
    void _initPoints();

    void _rotatePoint(Point &p);

public:
    Point p0;
    Point p1;
    Point p2;
    Point p3;
    Point pos;
    int rot;
    Type type;

    // for collision
    bool drop;
    bool trLeft;
    bool trRight;
    bool landed;
    float hold;
};

