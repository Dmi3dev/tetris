/*
 * Copyright (c) 2015 Dmitriy Shestakov
 * This file is part of tetris_made_for_practice.
 *
 * tetris_made_for_practice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */


#pragma once


#include <sys/time.h>
#include "sys/types.h"

class Timer
{
    timeval _nowTime;
    double _curTime;
    double _lastTime;
    double _elapsed;
    unsigned int _fpsCount;

public:
    // public static variable
    static unsigned int FPS;

    // constructor
    Timer();
    ~Timer();

    // methods
    void tick(double lock_fps=0.0);
    double getCurTime();
};




