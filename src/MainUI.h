/*
 * Copyright (c) 2015 Dmitriy Shestakov
 * This file is part of tetris_made_for_practice.
 *
 * tetris_made_for_practice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */


#pragma once

#include "Engine.h"
#include <QWidget>
#include <QPaintEvent>
#include <QKeyEvent>
#include <QSpinBox>
#include <QPainter>
#include <QPushButton>


class MainUI : public QWidget
{
Q_OBJECT

    Game *_game;
    QSpinBox *_level;
    QPushButton *_helpBtn;
    float _scorePosX;
    float _scorePosY;
    float _scoreSize;
    int _score;
    float _scoreOpacity;
    int _curKey;
    bool _clockwise;

public:
    // constructor
    MainUI(Game *game, QWidget *parent=NULL);
    ~MainUI();

private:
    // events
    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent *evt);
    void keyReleaseEvent(QKeyEvent *evt);
    void mousePressEvent(QMouseEvent *mouseEvent) override;
    void timerEvent(QTimerEvent *);

    void moveScore();
    void paintScore(QPainter &p);
    void paintButtons(QPainter &p);
    void paintButtonsPress(QPainter &p);
    void paintLevelUp(QPainter &p);

private slots:
    void showHelp();
    void start();
    void pause();
};


